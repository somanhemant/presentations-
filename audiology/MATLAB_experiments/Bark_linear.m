clc;
clearvars;
close all;


fs = 16000;
%fb = [50, 150, 250, 350, 450, 570, 700, 840, 1000, 1170, 1370, 1600, 1850, 2150, 2500, 2900, 3400, 4000];
fb = [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200, 2400, 2600, 2800, 3000, 3200, 3400, 3600, 3800, 4000];

[x, fs_s] = audioread('mix_5sec.wav');
x = x(1:40000);

N = 3;
rip = 10;

ch = length(fb) + 1;

%LPF
[B(1,:), A(1,:)] = cheby2(2*N, rip, fb(1)/(fs/2), 'low');

%BPF
for i = 2:(ch-1)
    [B(i,:), A(i,:)] = cheby2(N, rip, [(fb(i-1))/(fs/2) fb(i)/(fs/2)]);
end

%HPF
[B(ch,:), A(ch,:)] = cheby2(2*N, rip, fb(ch-1)/(fs/2), 'high');


Nfft = 2048;
for i = 1:ch
    [H(i,:), f] = freqz(B(i,:), A(i,:), Nfft, fs);
end
figure;
%plot(f, abs(H));
semilogx(f, 20*log10(abs(H)));


for i = 1:ch
    xch = filtfilt(B(i,:), A(i,:), x);

    s(i,:) = filtfilt(B(i,:), A(i,:), xch);
    s(i,:) = s(i,:)*((sqrt(mean(xch .^2)))/(sqrt(mean(s(i,:) .^2))));
end

    %v0 = sum(s0);
    v = sum(s);

%===================================================

fb = [50, 150, 250, 350, 450, 570, 700, 840, 1000, 1170, 1370, 1600, 1850, 2150, 2500, 2900, 3400, 4000];
%fb = [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200, 2400, 2600, 2800, 3000, 3200, 3400, 3600, 3800, 4000];

N = 3;
rip = 10;

ch = length(fb) + 1;

%LPF
[B(1,:), A(1,:)] = cheby2(2*N, rip, fb(1)/(fs/2), 'low');

%BPF
for i = 2:(ch-1)
    [B(i,:), A(i,:)] = cheby2(N, rip, [(fb(i-1))/(fs/2) fb(i)/(fs/2)]);
end

%HPF
[B(ch,:), A(ch,:)] = cheby2(2*N, rip, fb(ch-1)/(fs/2), 'high');


Nfft = 2048;
for i = 1:ch
    [H(i,:), f] = freqz(B(i,:), A(i,:), Nfft, fs);
end
figure;
%plot(f, abs(H));
semilogx(f, 20*log10(abs(H)));


for i = 1:ch
    xch = filtfilt(B(i,:), A(i,:), x);

    s0(i,:) = filtfilt(B(i,:), A(i,:), xch);
    s0(i,:) = s0(i,:)*((sqrt(mean(xch .^2)))/(sqrt(mean(s0(i,:) .^2))));
end

    v0 = sum(s0);
    %v = sum(s);
%===================================================

    figure;
    subplot(3,1,1);
    plot(x);
    subplot(3,1,2);
    plot(v0);
    subplot(3,1,3);
    plot(v);

    [Hlpftx, f] = freqz(x, Nfft, fs);
    [Hlpft0, f] = freqz(v0, Nfft, fs);
    [Hlpft, f] = freqz(v, Nfft, fs);

    figure;
    subplot(3,1,1);
    plot(f,abs(Hlpftx));
    subplot(3,1,2);
    plot(f,abs(Hlpft0));
    subplot(3,1,3);
    plot(f,abs(Hlpft));


    sound(v0, fs_s);
    pause(6);
    sound(v, fs_s);
