clc;
clearvars;
close all;


fs = 16000;
fb = [125, 250, 375, 500, 625, 750, 875, 1000, 1250, 1500, 1750, 2000, 2250, 3000, 3500, 4000];

[x, fs_s] = audioread('tone_5.5kHz_44.1kHz.wav');
x = resample(x, fs, fs_s);


%     bands = 4000;
%     for i = 1:bands
%         x_fft = fft(x((i-1)*40000/bands+1:i*40000/bands,1), 2*40000/bands);
%         x_ifft = ifft(x_fft);
%         x_inv((i-1)*40000/bands+1:i*40000/bands) = x_ifft(1:40000/bands);
%     end


    x_fft_full = fft(x,80000);
    x_fft_zero(:,1) = x_fft_full(:,1);
    for i = 25001:40000
        x_fft_zero(i,1) = 0;
    end
    for i = 40002:80000
        x_fft_zero(i,1) = conj(x_fft_zero(80002-i,1));
    end

    x_zero = ifft(x_fft_zero);




    %x_fft_re = x_fft_full(1:20001,1);
    %x_fft_re = resample(x_fft_re, fs_s/2, fs_s);
    x_fft_re(1:80000,1) = 0;

    for i = 1:20000
        x_fft_re(i,1) = x_fft_full(i,1);
    end
    for i = 20001:25000
        x_fft_re(i,1) = (x_fft_full(20001+4*(i-20001),1)+x_fft_full(20002+4*(i-20001),1)+x_fft_full(20003+4*(i-20001),1)+x_fft_full(20004+4*(i-20001),1))/4;
        %x_fft_re(i,1) = (x_fft_full(20001+2*(i-20001),1)+x_fft_full(20002+2*(i-20001),1)+x_fft_full(20003+2*(i-20001),1))/3;
    end
    x_fft_re(20001:25000,1) = x_fft_re(20001:25000,1)*((sqrt(mean(x_fft_full(20001:40000,1) .^2)))/(sqrt(mean(x_fft_re(20001:25000,1) .^2))));
    for i = 40002:80000
        x_fft_re(i,1) = conj(x_fft_re(80002-i,1));
    end

    fc = 6000;
    Nlpf = 4;
    [Blpf, Alpf] = butter(Nlpf, fc/(fs/2));


    x_re = ifft(x_fft_re);
%     x_re(1:80000,1) = x_re(1:80000,1)*((sqrt(mean(x(1:80000,1) .^2)))/(sqrt(mean(x_re(1:80000,1) .^2))));

    x_lpf = filtfilt(Blpf, Alpf, x_re);

    figure;
    subplot(3,1,1);
    plot(abs(x_fft_full));
    subplot(3,1,2);
    plot(abs(x_fft_zero));
    subplot(3,1,3);
    plot(abs(x_fft_re));

    figure;
    subplot(3,1,1);
    plot(x);
    subplot(3,1,2);
    plot(x_zero);
    subplot(3,1,3);
    plot(x_re);

    sound(x, fs);
    pause(6);
    sound(x_zero, fs);
    pause(6);
    sound(x_re, fs);
