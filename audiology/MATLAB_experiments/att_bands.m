clc;
clearvars;
close all;


fs = 16000;
fb = [125, 250, 375, 500, 625, 750, 875, 1000, 1250, 1500, 1750, 2000, 2250, 3000, 3500, 4000];

%att = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
att = [1, 1, 1, 1, 1, 1, 1, 0.56, 0.1, 0.032, 0.018, 0.01, 0.018, 0.032, 0.032, 0.032, 0.1];

[x, fs_s] = audioread('male_5sec.wav');
[n, fs_n] = audioread('noise_5sec.wav');
if fs_s ~= fs_n
    n = resample(n, fs_s, fs_n);
end
n = n(1:length(x));


N = 3;
rip = 20;

ch = length(fb) + 1;

%LPF
[B(1,:), A(1,:)] = cheby2(2*N, rip, fb(2)/(fs/2), 'low');

%BPF
for i = 3:(ch-1)
    [B(i,:), A(i,:)] = cheby2(N, rip, [fb(i-2)/(fs/2) fb(i)/(fs/2)]);
end

%HPF
[B(ch,:), A(ch,:)] = cheby2(2*N, rip, fb(ch-2)/(fs/2), 'high');


Nfft = 2048;
for i = 1:ch
    [H(i,:), f] = freqz(B(i,:), A(i,:), Nfft, fs);
end
%figure;
%plot(f, abs(H));
%semilogx(f, 20*log10(abs(H)));


fc = 40;
Nlpf = 4;
[Blpf, Alpf] = butter(Nlpf, fc/(fs/2));

[Hlpf, f] = freqz(Blpf, Alpf, Nfft, fs);
%figure;
%semilogx(f, 20*log10(abs(Hlpf)));


for i =1:ch
    if (i == 2)
        continue;
    end

    xch = filtfilt(B(i,:), A(i,:), x)*att(i);
    nch = filtfilt(B(i,:), A(i,:), n);

    %xch(find(xch<0)) = 0;
    for j = 1:size(xch)
        if(xch(j) < 0)
            xch(j) = 0;
        end
        if(nch(j) < 0)
            nch(j) = 0;
        end
    end

    e = filtfilt(Blpf, Alpf, xch);
    m = e .* nch;

    s0(i,:) = m;
    s0(i,:) = s0(i,:)*(sqrt(mean(xch .^2)))/(sqrt(mean(s0(i,:) .^2)));

    s(i,:) = filtfilt(B(i,:), A(i,:), xch);
    s(i,:) = s(i,:)*((sqrt(mean(xch .^2)))/(sqrt(mean(s(i,:) .^2))));
    s1(i,:) = s(i,:)/att(i);
end

    v0 = sum(s0);
    v = sum(s);
    v1 = sum(s1);

    figure;
    subplot(4,1,1);
    plot(x);
    subplot(4,1,2);
    plot(v0);
    subplot(4,1,3);
    plot(v);
    subplot(4,1,4);
    plot(v1);

    [Hlpftx, f] = freqz(x, Nfft, fs);
    [Hlpft0, f] = freqz(v0, Nfft, fs);
    [Hlpft, f] = freqz(v, Nfft, fs);
    [Hlpft1, f] = freqz(v1, Nfft, fs);

    figure;
    subplot(4,1,1);
    plot(f,abs(Hlpftx));
    subplot(4,1,2);
    plot(f,abs(Hlpft0));
    subplot(4,1,3);
    plot(f,abs(Hlpft));
    subplot(4,1,4);
    plot(f,abs(Hlpft1));


    sound(v, fs_s);
    pause(6);
    sound(v1, fs_s);
