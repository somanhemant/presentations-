clc;
clearvars;
close all;


bark = 1;

fs = 16000;
if (bark == 1)
    fb = [125, 250, 500, 1000, 2000, 4000];
else
    fb = [100, 880, 1660, 2440, 3220, 4000];
end

add = 3;
if (bark == 1)
    for i = 1:add
        base_fb = fb;
        for j = 1:length(base_fb)
            fb(2*j-1) = base_fb(j);
            if(j ~= length(base_fb))
                fb(2*j) = sqrt(base_fb(j)*base_fb(j+1));
            end
        end
    end
else
    for i = 1:add
        base_fb = fb;
        for j = 1:length(base_fb)
            fb(2*j-1) = base_fb(j);
            if(j ~= length(base_fb))
                fb(2*j) = (base_fb(j)+base_fb(j+1))/2;
            end
        end
    end
end

% [temp, fs_s] = audioread('mix_5sec.wav');
% x = temp(1:40000,1);
[x, fs_s] = audioread('male_5sec.wav');
[n, fs_n] = audioread('noise_5sec.wav');
if fs_s ~= fs_n
    n = resample(n, fs_s, fs_n);
end
n = n(1:length(x));


N = 3;
rip = [40 31 50; 25 29 33; 16 29 22; 13 28 15];
if(add > 3)
    add = 3;
end

ch = length(fb) + 1;

%LPF
[B(1,:), A(1,:)] = cheby2(2*N, rip(add+1,1), fb(2)/(fs/2), 'low');

%BPF
for i = 3:(ch-1)
    [B(i,:), A(i,:)] = cheby2(N, rip(add+1,2), [fb(i-2)/(fs/2) fb(i)/(fs/2)]);
end

%HPF
[B(ch,:), A(ch,:)] = cheby2(2*N, rip(add+1,3), fb(ch-2)/(fs/2), 'high');


Nfft = 2048;
for i = 1:ch
    [H(i,:), f] = freqz(B(i,:), A(i,:), Nfft, fs);
end
figure;
%plot(f, abs(H));
semilogx(f, 20*log10(abs(H)));


fc = 40;
Nlpf = 4;
[Blpf, Alpf] = butter(Nlpf, fc/(fs/2));

[Hlpf, f] = freqz(Blpf, Alpf, Nfft, fs);
figure;
semilogx(f, 20*log10(abs(Hlpf)));


for i = 1:ch
    if (i == 2)
        continue;
    end
    xch = filtfilt(B(i,:), A(i,:), x);
    nch = filtfilt(B(i,:), A(i,:), n);

    %xch(find(xch<0)) = 0;
    for j = 1:size(xch)
        if(xch(j) < 0)
            xch(j) = 0;
        end
        if(nch(j) < 0)
            nch(j) = 0;
        end
    end

    e = filtfilt(Blpf, Alpf, xch);

    %m = e .* nch;
    for j = 1:length(e)
        m(j,1) = e(j,1) * nch(mod(i+j,length(e))+1,1);
    end

    s0(i,:) = e;
    s0(i,:) = s0(i,:)*(sqrt(mean(xch .^2)))/(sqrt(mean(s0(i,:) .^2)));

    s(i,:) = filtfilt(B(i,:), A(i,:), m);
    s(i,:) = s(i,:)*((sqrt(mean(xch .^2)))/(sqrt(mean(s(i,:) .^2))));
end

    v0 = sum(s0);
    v = sum(s);

    figure;
    subplot(3,1,1);
    plot(x);
    subplot(3,1,2);
    plot(v0);
    subplot(3,1,3);
    plot(v);

    [Hlpftx, f] = freqz(x, Nfft, fs);
    [Hlpft0, f] = freqz(v0, Nfft, fs);
    [Hlpft, f] = freqz(v, Nfft, fs);

    figure;
    subplot(3,1,1);
    plot(f,abs(Hlpftx));
    subplot(3,1,2);
    plot(f,abs(Hlpft0));
    subplot(3,1,3);
    plot(f,abs(Hlpft));


    %sound(x, fs_s);
    %pause(6);
    sound(v, fs_s);
