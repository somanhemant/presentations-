clc;
clearvars;
close all;


fs = 16000;
fb = [50, 150, 250, 350, 450, 570, 700, 840, 1000, 1170, 1370, 1600, 1850, 2150, 2500, 2900, 3400, 4000];

[x, fs_s] = audioread('mix_5sec.wav');
x = x(1:40000);

N = 3;
rip = 10;

ch = length(fb) + 1;

%LPF
[B(1,:), A(1,:)] = cheby2(2*N, rip, fb(1)/(fs/2), 'low');

%BPF
for i = 2:(ch-1)
    [B(i,:), A(i,:)] = cheby2(N, rip, [(fb(i-1))/(fs/2) fb(i)/(fs/2)]);
end

%HPF
[B(ch,:), A(ch,:)] = cheby2(2*N, rip, fb(ch-1)/(fs/2), 'high');


Nfft = 2048;
for i = 1:ch
    [H(i,:), f] = freqz(B(i,:), A(i,:), Nfft, fs);
end
figure;
%plot(f, abs(H));
semilogx(f, 20*log10(abs(H)));


fc = 40;
Nlpf = 4;
[Blpf, Alpf] = butter(Nlpf, fc/(fs/2));

[Hlpf, f] = freqz(Blpf, Alpf, Nfft, fs);
figure;
semilogx(f, 20*log10(abs(Hlpf)));


for i = 1:ch
    if (mod(i,3) ~= 0)
        continue;
    end

    xch = filtfilt(B(i,:), A(i,:), x);


    s(i,:) = filtfilt(B(i,:), A(i,:), xch);
    s(i,:) = s(i,:)*((sqrt(mean(xch .^2)))/(sqrt(mean(s(i,:) .^2))));
end

    v = sum(s);

    [Hlpftx, f] = freqz(x, Nfft, fs);
    [Hlpft, f] = freqz(v, Nfft, fs);

    figure;
    subplot(2,1,1);
    plot(f,abs(Hlpftx));
    subplot(2,1,2);
    plot(f,abs(Hlpft));

    figure;
    subplot(2,1,1);
    plot(x);
    subplot(2,1,2);
    plot(v);

    sound(v, fs_s);
    pause(6);
    sound(x, fs_s);
